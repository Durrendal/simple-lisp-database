;;Haiku Generator
;;Inspired by Kenneth Faircloth Jr. <https://rootdoctorpoetry.wordpress.com/>
;;Author: Will Sinatra <wpsinatra@gmail.com>

;;===== Variables =====
(declaim (type (integer) *l1-moras*)
	 (type (integer) *l2-moras*)
	 (type (integer) *l3-moras*))

(defvar *l1-moras* 5)
(defvar *l2-moras* 7)
(defvar *l3-moras* 5)

;;===== Persistence =====
(load "interop.lisp")

(mkschm *ywords* (list :yword :syllables))

;;===== Functions =====
(defun containsp (str1 str2)
  (unless (string-equal str2 "")
    (loop for p = (search str2 str1) then (search str2 str1 :start2 (1+ p))
       while p
       collect p)))

(defun count-syllables (word)
  (let
      ((syllables 0))
    (loop for character across "yaeiou"
       do
	 (progn
	   (let
	       ((containsv? (not (containsp word (string character))))
		(amtofv (containsp word (string character))))
	     (if (equal (string character) "y")
		 (from *ywords* (concatenate 'string ":" word))
		 (if (equal containsv? NIL)
		     (setf syllables (+ syllables (length amtofv))))))))
(format t "~a~%" syllables)))
