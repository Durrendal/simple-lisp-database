;;===== Lazy Init =====
(ql:quickload "alexandria")

;;===== Variables =====
(declaim (type (string) *db*))
(declaim (type (string) *conf*))

(defvar *db* "database.lisp")
(defvar *conf* "db-conf.lisp")

(defparameter *schema* (list))
(defparameter *tables* (list))
(defparameter *persistent* (list))

;;===== Utility =====

(defmacro print-full (table)
  `(concatenate 'string "(defparameter " (string (quote ,table)) " '" (write-to-string ,table) ")"))

(defmacro mkpersistent (table)
  `(concatenate 'string "(persist " (string (quote ,table)) ")"))

(defmacro mkvar (table &optional tail state)
  `(concatenate 'string "(defvar " (string (quote ,table)) ,tail " " ,state ")"))

(defmacro mkstring (table tail)
  `(concatenate 'string (string (quote ,table)) ,tail))

(defun read-in-parameter (arg)
  (with-input-from-string (s arg)
    (eval (read s))))

;;===== Functions =====
(defmacro into (table param val)
  "(into *table* :key value)"
  `(push (list ,param ,val) ,table))

(defun from (table query)
  "(from *table* :col)"
  (alexandria:assoc-value table query))

(defmacro where (table key query)
  `(progn
     (let
	 ((tocheck ,table)
	  (schemamap (car (from *schema* (quote ,table)))))
       (let
	   ((position-of-col (position (quote ,key) schemamap)))
	 (dotimes (i (length ,table))
	   (let
	       ((current (pop tocheck)))
	     (if (equal (search ,query (nth position-of-col current)) 0)
		 (format t "~a~%" current))))))))
  
(defmacro del (table query)
  "(del *table* :col)"
  `(multiple-value-bind (info data)
       (from ,table ,query)
     (format NIL "~a" info)
     (setf ,table (remove data ,table :test 'equal))))

;; ersatz is used because this is a bad substitution method since it removes data in the database and pushes a new insert
(defmacro ersatz (table query newval)
  "(ersatz *table* :col 'new value')"
  `(progn
     (del ,table ,query)
     (into ,table ,query ,newval)))

(defmacro defschm (table &optional persistent)
  `(if (equal ,persistent t)
       (progn
	 (defparameter ,table (list))
	 ;(let
	    ; ((table-id (mkstring ,table "id")))
	   ;(read-in-parameter (mkvar (princ table-id)))
	   ;(setf (princ table-id) 0))
	 (push (mkpersistent ,table) *persistent*))
       (defparameter ,table (list))))

(defmacro recschm (table paramlist)
  "(recschm *table* (list :col1 :col2 ...))"
  `(progn
     (into *schema* (quote ,table) ,paramlist)
     (push (quote ,table) *tables*)))

(defmacro mkschm (table params)
  "(mkschm *table* (list :col1 :col2 ...)"
  `(progn
     (defschm ,table t)
     (recschm ,table ,params)))

(defmacro persist (table &optional (write-func :append))
  "(persist *table*) additioanlly you can specific :supersede to overwrite the current database file"
  `(with-open-file
       (stream *db* :direction :output :if-exists ,write-func)
     (format stream "~a~%" (print-full ,table))))

(defun persist-all ()
  (persist *schema* :supersede)
  (persist *tables*)
  (persist *persistent*)
  (let
      ((all-prst *persistent*))
    (dotimes (i (length *persistent*))
      (read-in-parameter (pop all-prst)))))
  
  
