# Simple Lisp Database

A simple persistent store system that creates and modifies (defparameter)'s

## Compiling:

The system is meant to function from the REPL, compilation is as simple as

sbcl --load interop.lisp
(save-lisp-and-die "simple-db" :executable t)

## Useage:

Load the database:
```
(load *db*)
```
alternately you can do
```
(load #P"/path/to/db")
```
*db* defaults to database.lisp in the current working directory, before compiling you can change this with (setf *db* "/path/to/db") if so desired.

Creating Schemas/Tables:

mkschm will create a persistent table, and record it to the table and schema segment of the database.

```
(mkschm *vpn* (list :site :ip))
```

mkschm is a macro that leverages two other macros, defschm & recschm
If you'd like to create an ephemeral table, you can do so with defschm. These ephemeral tables are not recorded in the *schema* or *tables* defparameter.

```
(defschm *tablename*)
```

If you'd like to manually add those ephemeral tables to the *schema* & *tables* you can do so with:

```
(recschm *tablename* (list :key (list params)))

or

(recschm *tablename* (list :key "data"))
```

Inserting data:

You can insert data into an existing table with into

Assuming you have the following Schemas defined

(*VPN* :SITE :IP)
(*CLIENTS* :NAME :CONTRACT :COVERAGE)

```
(into *vpn* :office-a "10.0.0.123")

(into *clients* :client-a (list "Supported" "Servers/Workstations"))
```

Replacing Data:

It looks like I accidentally put 10.0.0.123 as the IP for Office-a, I had meant to use 192.168.0.123.

```
(ersatz *vpn* :office-a "192.168.0.123")
```

would replace the address "10.0.0.123 in the *vpn* table with "192.168.0.123 for Office-A.

Deleting Data:

Sometimes we just need to get rid of things, we can achieve this easily with del:

```
(del *vpn* :office-a)
```

would delete :office-a from the *vpn* table.
Delete functions act against the entire key, and cannot be used to remove a value from an entry, while still leaving the entry.

Persisting Data:

When we've made our changes, we need to flush them to the disc, we can do so with

```
(persist-all)
```

Which will record all persistent tables/schemas to the file defined in *db*. Any epehmeral tables will be lost on exit.

If we need to manually record databases, or perhaps move them to a different file we can do so with the persist macro.

```
(persist *tablename*)
```

By default persist will append the data to the database file defined in *db*, we can overwrite that with

```
(persist *tablename* :supersede)
```

If we wanted to save a table to a new file we would do so by first setting *db* to a new value, then we could either persist-all to gather all of the persitent tables, or call persist manually on what we need to save.

```
(setf *db* "/new/file")

(persist-all)
```

if we need to manually add an epehmeral table, we would:

```
(setf *db* "/new/file")

(recschm *ephemeral-table* (list :key :data))
(recschm *ephemeral-table2* (list :key :col1 :col2 :col3))

(persist-all)
```

Persist-all functions by calling the persist macro on *schemas* *tables* and *persistent* then for each atom in *persistent* we call a persist function.

## Technical Breakdown:

### Variables:

*db*   : : Path to databse file, default bound to ./database.lisp
*conf* : : Currently unused

### Defparameters:

*schema*     : : Table Schema Definitions

*tables*     : : List of Tables

*persistent* : : List of Persistent Tables, each atom is a (persist *tablename*) string

### Macros:

print-full   : : Takes *table-name* as an argument, creates a (defparameter statement containing the full data of the *table-name* provided.

mkpersistent : : Takes *table-name* as an argument, creates a (persist *table-name*) string in *persistent*

into 	     : : Inserts an alist to the defined *table-name*, takes *table-name* and a string/list as arguments.

from 	     : : Selects data from table by key, takes *table-name* and :key as arguments.

del 	     : : Delets data from table by key, takes *table-name* and :key as arguments

ersatz 	     : : Replaces data in table by key, takes *table-name* and replacement string/list as arguments

defschm      : : Defines a schema & creates a blank (defparameter) with the defined *table-name*, takes *table-name* as arguments, and t as an optional argument to make the table persistent

recschm      : : Records a defined schema to *schema*, takes *table-name* and (list :key :col1 :col2 ...) as arguments

mkschm 	     : : Creates a table, records it to schema and makes it persistent, takes *table-name* (list :key :col1 :col2 ...) as arguments

persist      : : Flushed table data to file defined in *db*, takes *table-name* as argument

### Functions:

read-in-parameter : : Reads in string, and evaluates, takes a string as an argument.

persist-all : : Supersedes existing database with (persist *schema* :supersede), then persists *tables* & *persistent*, finally runs read-in-parameter on each atom of *persistent* to make all persisten tables flush to disc.


